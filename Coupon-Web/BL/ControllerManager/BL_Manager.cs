﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Coupon_Web.DAL;
using System.Data.SqlClient;
using System.Data;

namespace BL
{
    public class BL_Manager : IBL_Manager
    {
        private Queries _query;

        public BL_Manager()
        {
            _query = new Queries();
        }
        public int InsertBusiness(String name, String address, String description, String managerUserName, string latitude, string longitude, string city)
        {
            String id = (_query.SelectMax("Buisness", "Id") + 1).ToString();
            String[] values = new String[] { id, name, address, description, managerUserName, "false", latitude, longitude, city };
            _query.Insert("Buisness", values);
            return int.Parse(id);
        }
        public int InsertCoupon(String name, double originalMoney, double discountPrice, String expiredDate, int rating, int businessId, int category)
        {
            DateTime curDate = DateTime.Today.Date;
            String curDateStr = curDate.Month + "-" + curDate.Day + "-" + curDate.Year;
            String id = (_query.SelectMax("Coupon", "Id") + 1).ToString();
            String[] values = new String[] { id, name, originalMoney.ToString(), discountPrice.ToString(), expiredDate, rating.ToString(), businessId.ToString(), "False", curDateStr };
            _query.Insert("Coupon", values);
            InsertCouponCategory(int.Parse(id), category);
            return int.Parse(id);
        }
        public void InsertCouponCategory(int couponId, int categoryId)
        {
            String[] values = new String[] { couponId.ToString(), categoryId.ToString() };
            _query.Insert("CouponCategories", values);
        }
        public void DeleteBusiness(int id)
        {
            String[] pkNames = new String[] { "Id" };
            String[] pkValues = new String[] { id.ToString() };
            _query.Delete("Buisness", pkNames, pkValues);
        }
        public void DeleteCoupon(int couponId)
        {
            String[] pkNames = new String[] { "Id" };
            String[] pkValues = new String[] { couponId.ToString() };
            _query.Delete("Coupon", pkNames, pkValues);
        }
        public void EditCoupon(int id, String fieldToEdit, Object value)
        {
            String[] pkNames = { "Id" };
            String[] pkValues = new String[] { id.ToString() };
            _query.EditField("Coupon", pkNames, pkValues, fieldToEdit, value.ToString());
        }
        public DataTable selectCouponsName(String couponName)
        {
            String[] colums = { "[Coupon].Id", "[Coupon].Name", "DiscountPrice as 'Price'", "[Buisness].Name as 'Business'", "ExpiredDate", "Rating" };
            String[] pkNames = { "[Coupon].Name", "[Coupon].approve", "[Buisness].approve" };
            String[] pkValues = { couponName, "True", "True" };
            String[] tableNames = { "Coupon", "Buisness" };
            Tuple<String, String>[] intersect = new Tuple<string, string>[1];
            intersect[0] = new Tuple<string, string>("[Coupon].BuisnessId", "[Buisness].Id");
            return _query.select(tableNames, pkNames, pkValues, colums, intersect);
        }
        public DataTable selectCouponsDetails(String managerUserName)
        {
            String[] colums = { "[Coupon].Id", "[Coupon].Name", "DiscountPrice as 'Price'", "[Buisness].Name as 'Business'", "ExpiredDate" , "Rating"};
            String[] tableNames = { "Coupon", "Buisness" };
            Tuple<String, String>[] intersect = new Tuple<string, string>[1];
            intersect[0] = new Tuple<string, string>("[Coupon].BuisnessId", "[Buisness].Id");
            if (managerUserName.CompareTo("") == 0)
            {
                String[] pkNames = { "[Coupon].approve", "[Buisness].approve" };
                String[] pkValues = { "True", "True" };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }
            else
            {
                String[] pkNames = { "[Coupon].approve", "[Buisness].approve", "[Buisness].ManagerUserName" };
                String[] pkValues = { "True", "True", managerUserName };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }

        }

        public DataTable selectCouponDetailsWithCategory(String categoryName, String managerUserName)
        {

            String[] colums = { "[Coupon].Id", "[Coupon].Name", "DiscountPrice as 'Price'", "BuisnessId as 'Business'", "ExpiredDate", "[Category].Name as 'Category'", "Rating" };
            if (managerUserName.CompareTo("") == 0)
            {
                String[] pkNames = { "[Category].Name", "[Coupon].approve" };
                String[] pkValues = { categoryName, "True" };
                String[] tableNames = { "Coupon", "Category", "CouponCategories" };
                Tuple<String, String>[] intersect = new Tuple<string, string>[2];
                intersect[0] = new Tuple<string, string>("[Category].Id", "[CouponCategories].CategoryId");
                intersect[1] = new Tuple<string, string>("[Coupon].Id", "[CouponCategories].CouponId");
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }
            else
            {
                String[] pkNames = { "[Category].Name", "[Coupon].approve", "[Buisness].ManagerUserName" };
                String[] pkValues = { categoryName, "True", managerUserName };
                String[] tableNames = { "Coupon", "Category", "CouponCategories", "Buisness" };
                Tuple<String, String>[] intersect = new Tuple<string, string>[3];
                intersect[0] = new Tuple<string, string>("[Category].Id", "[CouponCategories].CategoryId");
                intersect[1] = new Tuple<string, string>("[Coupon].Id", "[CouponCategories].CouponId");
                intersect[2] = new Tuple<string, string>("[Coupon].BuisnessId", "[Buisness].Id");
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }
        }

        public DataTable selectBusinessByCity(String cityName, String managerUserName)
        {
            String[] colums = { "[Buisness].Id", "[Buisness].Name", "[Buisness].Address", "[Buisness].Description", "[Buisness].City" };
            String[] tableNames = { "Buisness" };
            Tuple<String, String>[] intersect = new Tuple<string, string>[0];
            if (managerUserName.Length != 0)
            {
                String[] pkValues = { cityName, managerUserName, "True" };
                String[] pkNames = { "[Buisness].City", "[Buisness].ManagerUserName", "[Buisness].approve" };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }
            else
            {
                String[] pkValues = { cityName, "True" };
                String[] pkNames = { "[Buisness].City", "[Buiseness].approve" };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }

        }

        public DataTable selectBuisnesses(String managerName)
        {
            String[] colums = { "[Buisness].Name as 'title'", "[Buisness].Latitude as 'lat'", "[Buisness].Longitude as 'lng'", "[Buisness].Name as 'description'" };
            String[] tableNames = { "Buisness" };
            Tuple<String, String>[] intersect = new Tuple<string, string>[0];
            if (managerName.Length != 0)
            {
                String[] pkValues = { managerName, "True" };
                String[] pkNames = { "[Buisness].ManagerUserName", "[Buisness].approve" };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }
            else
            {
                String[] pkValues = { "True" };
                String[] pkNames = { "[Buisness].approve" };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }

        }

        public DataTable selectBuisnessesByName(String businessName, String managerName)
        {
            String[] colums = { "[Buisness].Name", "[Buisness].Address", "[Buisness].Description", "[Buisness].City" };
            String[] tableNames = { "Buisness" };
            Tuple<String, String>[] intersect = new Tuple<string, string>[0];
            if (managerName.Length != 0)
            {
                String[] pkValues = { managerName, businessName, "True" };
                String[] pkNames = { "[Buisness].ManagerUserName", "[Buisness].Name", "[Buisness].approve" };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }
            else
            {
                String[] pkValues = { businessName, "True" };
                String[] pkNames = { "[Buisness].Name", "[Buisness].approve" };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }
        }
        public DataTable selectCoupons(String managerUseName)
        {
            String[] colums = { "[Coupon].Name as 'title'", "[Buisness].Latitude as 'lat'", "[Buisness].Longitude as 'lng'", "[Coupon].Name as 'description'" };
            String[] tableNames = { "Buisness", "Coupon" };
            Tuple<String, String>[] intersect = new Tuple<string, string>[1];
            intersect[0] = new Tuple<string, string>("[Coupon].BuisnessId", "[Buisness].Id");
            if (managerUseName.CompareTo("") == 0)
            {
                String[] pkValues = { "True", "True" };
                String[] pkNames = { "[Coupon].approve", "[Buisness].approve" };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }
            else
            {
                String[] pkValues = { "True", "True", managerUseName };
                String[] pkNames = { "[Coupon].approve", "[Buisness].approve", "[Buisness].ManagerUserName" };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }
        }
        public String selectManagerOfCoupon(int couponId)
        {
            String[] colums = { "[Manager].UserName" };
            String[] pkNames = { "[Coupon].Id" };
            String[] pkValues = { couponId.ToString() };
            String[] tableNames = { "Coupon", "Buisness", "Manager" };
            Tuple<String, String>[] intersect = new Tuple<string, string>[2];
            intersect[0] = new Tuple<string, string>("[Coupon].BuisnessId", "[Buisness].Id");
            intersect[1] = new Tuple<string, string>("[Buisness].ManagerUserName", "[Manager].UserName");
            DataTable dt = _query.select(tableNames, pkNames, pkValues, colums, intersect);
            return (String)dt.Rows[0][0];
        }

        public String selectManagerOfBusiness(int businessId)
        {
            String[] colums = { "[Buisness].ManagerUserName" };
            String[] pkNames = { "[Buisness].Id" };
            String[] pkValues = { businessId.ToString() };
            String[] tableNames = { "Buisness" };
            Tuple<String, String>[] intersect = new Tuple<string, string>[0];
            DataTable dt = _query.select(tableNames, pkNames, pkValues, colums, intersect);
            return (String)dt.Rows[0][0];
        }

        public void EditBusiness(int id, String fieldToEdit, Object value)
        {
            String[] pkNames = new String[] { "[Buisness].Id" };
            String[] pkValues = new String[] { id.ToString() };
            _query.EditField("Buisness", pkNames, pkValues, fieldToEdit, value.ToString());
        }

        public DataTable getDetialFromCouponSerialKey(String key)
        {
            String[] tableNames = { "Deal", "Coupon", "Customer", "Users" };
            String[] colums = { "[Deal].Id as 'DealID'", 
                                "[Coupon].Name as 'Coupon Name'", 
                                "[Users].Name as 'Customer Name'"};
            String[] pkNames = { "[Deal].Status", "SerialKey"};
            String[] pkValues = { "False", key };
            
            Tuple<String, String>[] intersect = new Tuple<string, string>[3];
            intersect[0] = new Tuple<string, string>("[Deal].CustomerUserName", "[Customer].UserName");
            intersect[1] = new Tuple<string, string>("[Customer].UserName", "[Users].UserName");
            intersect[2] = new Tuple<string, string>("[Deal].CouponId", "[Coupon].Id");
            return _query.select(tableNames, pkNames, pkValues, colums, intersect);    
        }

        public void ImmplementCouponBySerial(String key)
        {
            String[] pKeyNames = { "SerialKey" };
            String [] pkValues = { key };
            _query.EditField("Deal", pKeyNames, pkValues, "Status", "True");
        }

    }

}
