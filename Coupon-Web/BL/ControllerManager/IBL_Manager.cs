﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BL
{
    public interface IBL_Manager
    {
        int InsertBusiness(String name, String address, String description, String managerUserName, string latitude, string longitude, string city);
        int InsertCoupon(String name, double originalMoney, double discountPrice, String expiredDate, int rating, int businessId, int category);
        void InsertCouponCategory(int couponId, int categoryId);
        void DeleteBusiness(int id);
        void DeleteCoupon(int couponId);
        void EditCoupon(int id, String fieldToEdit, Object value);
        DataTable selectCouponsName(String couponName);
        DataTable selectCouponsDetails(String managerUserName);
        DataTable selectCouponDetailsWithCategory(String categoryName, String managerUserName);
        DataTable selectBusinessByCity(String cityName, String managerUserName);
        DataTable selectBuisnesses(String managerName);
        DataTable selectBuisnessesByName(String businessName, String managerName);
        DataTable selectCoupons(String managerUseName);
        String selectManagerOfCoupon(int couponId);
        String selectManagerOfBusiness(int businessId);
        void EditBusiness(int id, String fieldToEdit, Object value);
        DataTable getDetialFromCouponSerialKey(String key);
        void ImmplementCouponBySerial(String key);
        
    }

}
