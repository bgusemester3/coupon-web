﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Coupon_Web.DAL;
using System.Data.SqlClient;
using System.Data;


namespace BL
{
  
    public class BL_Customer : IBL_Customer
    {
        private Queries _query;

        public BL_Customer()
        {
            _query = new Queries();
        }
        public int InsertDeal(int status, String serialKey, int couponId, String customerUserName, String paymentMethod)
        {
            String id = (_query.SelectMax("Deal", "Id") + 1).ToString();
            String[] values = new String[] { id, status.ToString(), serialKey, couponId.ToString(), customerUserName, paymentMethod };
            _query.Insert("Deal", values);
            return int.Parse(id);
        }
        public void InsertRates(String userName, int couponId, int rateNumber, String description)
        {
            String[] values = new String[] { userName, couponId.ToString(), rateNumber.ToString(), description };
            _query.Insert("Rates", values);
        }
        public DataTable selectCouponsName(String couponName)
        {
            String[] colums = { "[Coupon].Id", "[Coupon].Name", "DiscountPrice as 'Price'", "[Buisness].Name as 'Business'", "ExpiredDate", "Rating" };
            String[] pkNames = { "[Coupon].Name", "[Coupon].approve", "[Buisness].approve" };
            String[] pkValues = { couponName, "True", "True" };
            String[] tableNames = { "Coupon", "Buisness" };
            Tuple<String, String>[] intersect = new Tuple<string, string>[1];
            intersect[0] = new Tuple<string, string>("[Coupon].BuisnessId", "[Buisness].Id");
            return _query.select(tableNames, pkNames, pkValues, colums, intersect);
        }
        public DataTable selectCouponsDetails(String managerUserName)
        {
            String[] colums = { "[Coupon].Id", "[Coupon].Name", "DiscountPrice as 'Price'", "[Buisness].Name as 'Business'", "ExpiredDate" , "Rating"};
            String[] tableNames = { "Coupon", "Buisness" };
            Tuple<String, String>[] intersect = new Tuple<string, string>[1];
            intersect[0] = new Tuple<string, string>("[Coupon].BuisnessId", "[Buisness].Id");
            if (managerUserName.CompareTo("") == 0)
            {
                String[] pkNames = { "[Coupon].approve", "[Buisness].approve" };
                String[] pkValues = { "True", "True" };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }
            else
            {
                String[] pkNames = { "[Coupon].approve", "[Buisness].approve", "[Buisness].ManagerUserName" };
                String[] pkValues = { "True", "True", managerUserName };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }

        }

        public DataTable selectCouponDetailsWithCategory(String categoryName, String managerUserName)
        {

            String[] colums = { "[Coupon].Id", "[Coupon].Name", "DiscountPrice as 'Price'", "BuisnessId as 'Business'", "ExpiredDate", "[Category].Name as 'Category'", "Rating" };
            if (managerUserName.CompareTo("") == 0)
            {
                String[] pkNames = { "[Category].Name", "[Coupon].approve" };
                String[] pkValues = { categoryName, "True" };
                String[] tableNames = { "Coupon", "Category", "CouponCategories" };
                Tuple<String, String>[] intersect = new Tuple<string, string>[2];
                intersect[0] = new Tuple<string, string>("[Category].Id", "[CouponCategories].CategoryId");
                intersect[1] = new Tuple<string, string>("[Coupon].Id", "[CouponCategories].CouponId");
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }
            else
            {
                String[] pkNames = { "[Category].Name", "[Coupon].approve", "[Buisness].ManagerUserName" };
                String[] pkValues = { categoryName, "True", managerUserName };
                String[] tableNames = { "Coupon", "Category", "CouponCategories", "Buisness" };
                Tuple<String, String>[] intersect = new Tuple<string, string>[3];
                intersect[0] = new Tuple<string, string>("[Category].Id", "[CouponCategories].CategoryId");
                intersect[1] = new Tuple<string, string>("[Coupon].Id", "[CouponCategories].CouponId");
                intersect[2] = new Tuple<string, string>("[Coupon].BuisnessId", "[Buisness].Id");
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }
        }

        public DataTable selectBusinessByCity(String cityName, String managerUserName)
        {
            String[] colums = { "[Buisness].Id", "[Buisness].Name", "[Buisness].Address", "[Buisness].Description", "[Buisness].City" };
            String[] tableNames = { "Buisness" };
            Tuple<String, String>[] intersect = new Tuple<string, string>[0];
            if (managerUserName.Length != 0)
            {
                String[] pkValues = { cityName, managerUserName, "True" };
                String[] pkNames = { "[Buisness].City", "[Buisness].ManagerUserName", "[Buisness].approve" };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }
            else
            {
                String[] pkValues = { cityName, "True" };
                String[] pkNames = { "[Buisness].City", "[Buiseness].approve" };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }

        }

        public DataTable selectBuisnesses(String managerName)
        {
            String[] colums = { "[Buisness].Name as 'title'", "[Buisness].Latitude as 'lat'", "[Buisness].Longitude as 'lng'", "[Buisness].Name as 'description'" };
            String[] tableNames = { "Buisness" };
            Tuple<String, String>[] intersect = new Tuple<string, string>[0];
            if (managerName.Length != 0)
            {
                String[] pkValues = { managerName, "True" };
                String[] pkNames = { "[Buisness].ManagerUserName", "[Buisness].approve" };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }
            else
            {
                String[] pkValues = { "True" };
                String[] pkNames = { "[Buisness].approve" };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }

        }

        public DataTable selectBuisnessesByName(String businessName, String managerName)
        {
            String[] colums = { "[Buisness].Name", "[Buisness].Address", "[Buisness].Description", "[Buisness].City" };
            String[] tableNames = { "Buisness" };
            Tuple<String, String>[] intersect = new Tuple<string, string>[0];
            if (managerName.Length != 0)
            {
                String[] pkValues = { managerName, businessName, "True" };
                String[] pkNames = { "[Buisness].ManagerUserName", "[Buisness].Name", "[Buisness].approve" };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }
            else
            {
                String[] pkValues = { businessName, "True" };
                String[] pkNames = { "[Buisness].Name", "[Buisness].approve" };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }
        }
        public DataTable selectAllCoupons()
        {
            String[] colums = { };
            String[] pkNames = { };
            String[] pkValues = { };
            String[] tableNames = { "Coupon" };
            Tuple<String, String>[] intersect = new Tuple<string, string>[0];
            return _query.select(tableNames, pkNames, pkValues, colums, intersect);
        }
        public DataTable selectCoupons(String managerUseName)
        {
            String[] colums = { "[Coupon].Name as 'title'", "[Buisness].Latitude as 'lat'", "[Buisness].Longitude as 'lng'", "[Coupon].Name as 'description'" };
            String[] tableNames = { "Buisness", "Coupon" };
            Tuple<String, String>[] intersect = new Tuple<string, string>[1];
            intersect[0] = new Tuple<string, string>("[Coupon].BuisnessId", "[Buisness].Id");
            if (managerUseName.CompareTo("") == 0)
            {
                String[] pkValues = { "True", "True" };
                String[] pkNames = { "[Coupon].approve", "[Buisness].approve" };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }
            else
            {
                String[] pkValues = { "True", "True", managerUseName };
                String[] pkNames = { "[Coupon].approve", "[Buisness].approve", "[Buisness].ManagerUserName" };
                return _query.select(tableNames, pkNames, pkValues, colums, intersect);
            }
        }
        public DataTable selectCustomerHistory(String userName)
        {
            String[] colums = { "[Coupon].Name", "[Deal].PaymentMethod", "[Deal].SerialKey" };
            String[] pkNames = { "[Deal].CustomerUserName" };
            String[] pkValues = { userName };
            String[] tableNames = { "Deal", "Coupon" };
            Tuple<String, String>[] intersect = new Tuple<string, string>[1];
            intersect[0] = new Tuple<string, string>("[Deal].CouponId", "[Coupon].Id");
            return _query.select(tableNames, pkNames, pkValues, colums, intersect);
        }

        public bool IsRateExist(String customerUserName, int couponId)
        {
            String[] pkNames = { "customerUserName", "CouponId" };
            String[] pkValues = new String[] { customerUserName, couponId.ToString() };
            return _query.isExist("Rates", pkNames, pkValues);
        }


        public void removeCustomerCategoryPref(String userName, int catId)
        {
            String tableName = "CustomerPerferences";
            String[] pKeyValues = { userName, catId.ToString() };
            String[] pKeyNames = { "CustomerUserName", "CategoryId" };
            _query.Delete(tableName, pKeyNames, pKeyValues);
        }
        public void addCustomerCategoryPref(String userName, int catId)
        {
            String tableName = "CustomerPerferences";
            String[] values = { userName, catId.ToString() };
            _query.Insert(tableName, values);
        }
        public DataTable selectCustomerCategoryPref (String userName)
        {
            String [] tableNames = {"CustomerPerferences", "Category"};
            String[] pKeyNames = { "CustomerUserName"};
            String[] pKeyValues = { userName};
            String[] colums = { "Name as 'Category'" };
            Tuple<String, String>[] intersect = new Tuple<string, string>[1];
            intersect[0] = new Tuple<string, string>("[CustomerPerferences].CategoryId", "[Category].Id");
            return _query.select(tableNames, pKeyNames, pKeyValues, colums, intersect);
   
        }

        public DataTable selectCouponsWithCategory(String categoryName)
        {
            String[] tableNames = { "CouponCategories", "Coupon","Category" };
            String[] pKeyNames = { "[Category].Name" };
            String[] pKeyValues = { categoryName };
            String[] colums = { "[Coupon].Id", "[Coupon].Name", "DiscountPrice as 'Price'", "BuisnessId as 'Business'", "ExpiredDate", "[Category].Name as 'Category'", "Rating" };
            Tuple<String, String>[] intersect = new Tuple<string, string>[2];
            intersect[0] = new Tuple<string, string>("[CouponCategories].CouponId", "[Coupon].Id");
            intersect[1] = new Tuple<string, string>("[Category].Id", "[CouponCategories].CategoryId");
            return _query.select(tableNames, pKeyNames, pKeyValues, colums, intersect);
        }

    }


}
