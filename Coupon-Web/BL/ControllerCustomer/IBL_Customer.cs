﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BL
{
    public interface IBL_Customer
    {
        int InsertDeal(int status, String serialKey, int couponId, String customerUserName, String paymentMethod);
        void InsertRates(String userName, int couponId, int rateNumber, String description);
        DataTable selectCouponsName(String couponName);
        DataTable selectCouponsDetails(String managerUserName);
        DataTable selectCouponDetailsWithCategory(String categoryName, String managerUserName);
        DataTable selectBusinessByCity(String cityName, String managerUserName);
        DataTable selectBuisnesses(String managerName);
        DataTable selectBuisnessesByName(String businessName, String managerName);
        DataTable selectAllCoupons();
        DataTable selectCoupons(String managerUseName);
        DataTable selectCustomerHistory(String userName);
        DataTable selectCouponsWithCategory(String categoryName);
        bool IsRateExist(String name, int couponId);
        void removeCustomerCategoryPref(String userName, int catId);
        void addCustomerCategoryPref(String userName, int catId);
        DataTable selectCustomerCategoryPref(String userName);
    }
}
