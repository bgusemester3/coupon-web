﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System;
namespace BL
{
    public interface IBL_SystemManager
    {

        void EditBusiness(int id, String fieldToEdit, Object value);
        void DeleteBusiness(int id);
        void DeleteCoupon(int couponId);
        int InsertBusiness(String name, String address, String description, String managerUserName, string latitude, string longitude, string city);
        void EditCoupon(int id, String fieldToEdit, Object value);
        DataTable selectCouponsName(String couponName);
        DataTable selectCouponsDetails(String managerUserName);
        DataTable selectCouponDetailsWithCategory(String categoryName, String managerUserName);
        DataTable selectBusinessByCity(String cityName, String managerUserName);
        DataTable selectBuisnesses(String managerName);
        DataTable selectBuisnessesByName(String businessName, String managerName);
        DataTable selectCoupons(String managerUseName);
        DataTable selectCouponApprove();
        DataTable selectBusinessApprove(bool approveValue);
    }



}

