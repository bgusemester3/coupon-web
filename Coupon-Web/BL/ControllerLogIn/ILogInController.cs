﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BL
{
     public interface ILogInController
    {
        Boolean ExistUserForLogin(string userName, string password);
        bool IsCustomerExist(String userName);
        bool IsManagerExist(String userName);
        Boolean LoginAsCustomer(string userName, string password);
        Boolean LoginAsManager(string userName, string password);
        Boolean LoginAsSystemManager(string userName, string password);
        bool IsSystemManagerExist(String userName);
        void InsertCustomer(String userName, String email, String phone, String name, String password);
        void InsertManager(String userName, String name, String password);
        void InsertUser(String userName, String name, String password);
        bool IsUserExist(String userName);
        DataTable getUserPersonalName(String userName);
    }
}
