﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Coupon_Web.DAL;
using System.Data.SqlClient;
using System.Data;

namespace BL
{
    public class LogInController : ILogInController
    {
        private Queries _query;


        public LogInController()
        {
            _query = new Queries();
        }


        public Boolean ExistUserForLogin(string userName, string password)
        {
            String[] pkNames = { "UserName", "Password" };
            String[] pkValues = new String[] { userName, password };
            return _query.isExist("Users", pkNames, pkValues);
        }
        public bool IsCustomerExist(String userName)
        {
            String[] pkNames = { "UserName" };
            String[] pkValues = new String[] { userName };
            return _query.isExist("Customer", pkNames, pkValues);
        }
        public bool IsManagerExist(String userName)
        {
            String[] pkNames = { "UserName" };
            String[] pkValues = new String[] { userName };
            return _query.isExist("Manager", pkNames, pkValues);
        }
        public Boolean LoginAsCustomer(string userName, string password)
        {
            return ExistUserForLogin(userName, password) && IsCustomerExist(userName);
        }
        public Boolean LoginAsManager(string userName, string password)
        {
            return ExistUserForLogin(userName, password) && IsManagerExist(userName);
        }
        public Boolean LoginAsSystemManager(string userName, string password)
        {
            return ExistUserForLogin(userName, password) && IsSystemManagerExist(userName);
        }
        public bool IsSystemManagerExist(String userName)
        {
            String[] pkNames = { "UserName" };
            String[] pkValues = new String[] { userName };
            return _query.isExist("SystemManager", pkNames, pkValues);
        }
        public void InsertCustomer(String userName, String email, String phone, String name, String password)
        {
            InsertUser(userName, name, password);
            String[] values = new String[] { userName, email, phone };
            _query.Insert("Customer", values);
        }
        public void InsertManager(String userName, String name, String password)
        {
            InsertUser(userName, name, password);
            String[] values = new String[] { userName };
            _query.Insert("Manager", values);
        }
        public void InsertUser(String userName, String name, String password)
        {
            String[] values = new String[] { userName, name, password };
            _query.Insert("Users", values);
        }
        public bool IsUserExist(String userName)
        {
            String[] pkNames = { "UserName" };
            String[] pkValues = new String[] { userName };
            return _query.isExist("Users", pkNames, pkValues);
        }
        public DataTable getUserPersonalName(String userName)
        {
            String[] colums = { "[Users].Name" };
            String[] pkNames = { "[Users].UserName" };
            String[] pkValues = { userName };
            String[] tableNames = { "Users" };
            Tuple<String, String>[] intersect = new Tuple<string, string>[0];
            return _query.select(tableNames, pkNames, pkValues, colums, intersect);
        }
    }
}
