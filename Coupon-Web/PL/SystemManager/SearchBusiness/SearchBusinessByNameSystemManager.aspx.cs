﻿using BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.SystemManager.SearchBusiness
{
    public partial class SearchBusinessByNameSystemManager : System.Web.UI.Page
    {
        DataTable _dt;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (SiteMapDataSource1 != null)
            {
                SiteMapDataSource1.StartingNodeUrl = "~/SystemManager/SystemManagerHomeP.aspx";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            IBL_SystemManager systemManagerController = new BL_SystemManager();
            string text = TextBox1.Text;
            _dt = systemManagerController.selectBuisnessesByName(text, "");
            GridView1.DataSource = _dt;
            GridView1.DataBind();
        }

    }
}