﻿using BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.SystemManager.SearchBusiness
{
    public partial class SearchBusinessByCitySystemManager : System.Web.UI.Page
    {
        DataTable _dt;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SiteMapDataSource1 != null)
            {
                SiteMapDataSource1.StartingNodeUrl = "~/SystemManager/SystemManagerHomeP.aspx";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            initDt();
        }

        private void initDt()
        {
            IBL_SystemManager systemManagerController = new BL_SystemManager();
            _dt = systemManagerController.selectBusinessByCity(TextBox1.Text, "");
            GridView1.DataSource = _dt;
            GridView1.DataBind();
        }
    }
}