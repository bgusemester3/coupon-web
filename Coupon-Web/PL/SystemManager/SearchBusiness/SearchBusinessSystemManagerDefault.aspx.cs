﻿using BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.SystemManager.SearchBusiness
{
    public partial class SearchBusinessSystemManagerDefault : System.Web.UI.Page
    {
        DataTable _dt;
        IBL_SystemManager systemManagerController;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SiteMapDataSource1 != null)
            {
                SiteMapDataSource1.StartingNodeUrl = "~/SystemManager/SystemManagerHomeP.aspx";
            }
            _dt = new DataTable();
            systemManagerController = new BL_SystemManager();
            String user = (String)Session["User"];
            _dt = systemManagerController.selectBuisnesses("");
            GridView1.DataSource = _dt;
            GridView1.DataBind();

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}