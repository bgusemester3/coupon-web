﻿using BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.SystemManager
{
    public partial class EditCouponSystemManager : System.Web.UI.Page
    {
            protected void Page_Load(object sender, EventArgs e)
            {
                if (SiteMapDataSource1 != null)
                {
                    SiteMapDataSource1.StartingNodeUrl = "~/SystemManager/SystemManagerHomeP.aspx";
                }
            }

            protected void addCoupon_btn_Click(object sender, EventArgs e)
            {
                IBL_SystemManager systemManagerController = new BL_SystemManager();
                int couponId;
                try
                {
                    couponId = int.Parse(txtBoxCouponId.Text);
                    systemManagerController.EditCoupon(couponId, DropDownList1.Text, TextBox1.Text);
                    Response.Redirect(SiteMapDataSource1.StartingNodeUrl);
                }
                catch (Exception)
                {
                    errorlbl.Text = "Error,Please Check Out Your Parameters";
                }
            }
        }
    }