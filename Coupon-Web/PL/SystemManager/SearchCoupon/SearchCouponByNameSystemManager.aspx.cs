﻿using BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.SystemManager.SearchCoupon
{
    public partial class SearchCouponByNameSystemManager : System.Web.UI.Page
    {
        DataTable _dt;
        IBL_SystemManager systemManagerController;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (SiteMapDataSource1 != null)
            {
                SiteMapDataSource1.StartingNodeUrl = "~/SystemManager/SystemManagerHomeP.aspx";
            }
            _dt = new DataTable();
            systemManagerController = new BL_SystemManager();
            String user = (String)Session["User"];
            _dt = systemManagerController.selectCouponsDetails("");
             View.DataSource = _dt;
             View.DataBind();
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string text = TextBox_Name.Text;
            if (text.CompareTo("") == 0)
                _dt = systemManagerController.selectCouponsDetails("");
            else  _dt = systemManagerController.selectCouponsName(text);
            View.DataSource = _dt;
            View.DataBind();
        }

    }
}