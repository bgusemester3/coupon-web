﻿using BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.SystemManager.SearchCoupon
{
    public partial class SearchCouponByCategorySystemManager : System.Web.UI.Page
    {
        DataTable _dt;
        IBL_SystemManager systemManagerController;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (SiteMapDataSource1 != null)
            {
                SiteMapDataSource1.StartingNodeUrl = "~/SystemManager/SystemManagerHomeP.aspx";
            }
            systemManagerController = new BL_SystemManager();
            _dt = systemManagerController.selectCouponDetailsWithCategory(category.Text, "");
            View.DataSource = _dt;
            View.DataBind();
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            _dt = systemManagerController.selectCouponDetailsWithCategory(category.Text, "");
            View.DataSource = _dt;
            View.DataBind();
        }

    }
}