﻿using BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.SystemManager
{
    public partial class ApproveBusiness : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            IBL_SystemManager systemManagerController = new BL_SystemManager();
            DataTable dt = systemManagerController.selectBusinessApprove(false);
            GridView1.DataSource = dt;
            GridView1.DataBind();

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            IBL_SystemManager systemManagerController = new BL_SystemManager();
            int id;
            try
            {
                id = int.Parse(TextBox1.Text);
                errorlbl.Text = "";
                systemManagerController.EditBusiness(id, "approve", "True");
                DataTable dt = systemManagerController.selectBusinessApprove(false);
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            catch (Exception)
            {
                errorlbl.Text = "Invalid Number";
            }
        }
    }
}