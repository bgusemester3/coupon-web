﻿using BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.SystemManager
{
    public partial class DeleteCouponSystemManager : System.Web.UI.Page
    {
        private IBL_SystemManager systemManagerController;
        protected void Page_Load(object sender, EventArgs e)
        {
            systemManagerController = new BL_SystemManager();
            if (SiteMapDataSource1 != null)
            {
                SiteMapDataSource1.StartingNodeUrl = "~/SystemManager/SystemManagerHomeP.aspx";
            }
        }

        protected void addCoupon_btn_Click(object sender, EventArgs e)
        {
            int id = int.Parse(txtBoxID.Text);
            systemManagerController.DeleteCoupon(id);
        }
    }
}