﻿using BL;
using Coupon_Web.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.Manager
{
    public partial class ImmplementCoupon : System.Web.UI.Page
    {
        private String _key;
        DataTable _dt;
        IBL_Manager managerController;

        protected void Page_Load(object sender, EventArgs e)
        {
            managerController = new BL_Manager();
            if (SiteMapDataSource1 != null)
            {
                SiteMapDataSource1.StartingNodeUrl = "~/Manager/ManagerHomeP.aspx";
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            _key = serialKey.Text;
            _dt = managerController.getDetialFromCouponSerialKey(serialKey.Text);
            if(_dt.Rows.Count > 0)
            {
               
                View.DataSource = _dt;
                View.DataBind();
                detail.Visible = true;
                View.Visible = true;
                commit.Visible = true;
            }
            else
            {
                _key = "";
                detail.Visible = false;
                View.Visible = false;
                commit.Visible = false;
            }

        }

        protected void commit_Click(object sender, EventArgs e)
        {

            if (serialKey.Text.CompareTo("") != 0)
            {
                managerController.ImmplementCouponBySerial(serialKey.Text);
                _key = "";
                detail.Visible = false;
                View.Visible = false;
                commit.Visible = false;
            }
            
        }
    }
}