﻿using BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.Manager.SearchBusiness
{
    public partial class SearchBusinessByCityManager : System.Web.UI.Page
    {
        DataTable _dt;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SiteMapDataSource1 != null)
            {
               SiteMapDataSource1.StartingNodeUrl = "~/Manager/ManagerHomeP.aspx";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            initDt();
        }

        private void initDt()
        {
            IBL_Manager managerController = new BL_Manager();
            string user = (String)Session["User"];
            _dt = managerController.selectBusinessByCity(TextBox1.Text, (String)Session["UserName"]);
            GridView1.DataSource = _dt;
            GridView1.DataBind();
        }
    }
}