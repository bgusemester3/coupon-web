﻿using BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.Manager.SearchCoupon
{
    public partial class SearchCouponByNameManger : System.Web.UI.Page
    {
        DataTable _dt;
        IBL_Manager managerController;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (SiteMapDataSource1 != null)
            {
                SiteMapDataSource1.StartingNodeUrl = "~/Manager/ManagerHomeP.aspx";
            }
            _dt = new DataTable();
            managerController = new BL_Manager();
            String user = (String)Session["User"];
            if (user.CompareTo("Manager") == 0)
            {
                _dt = managerController.selectCouponsDetails((String)Session["UserName"]);
                View.DataSource = _dt;
                View.DataBind();
            }
            else
            {
                _dt = managerController.selectCouponsDetails("");
                View.DataSource = _dt;
                View.DataBind();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string text = TextBox_Name.Text;
            if (text.CompareTo("") == 0)
                _dt = managerController.selectCouponsDetails("");
            else 
                _dt = managerController.selectCouponsName(text);
            View.DataSource = _dt;
            View.DataBind();
        }

    }
}