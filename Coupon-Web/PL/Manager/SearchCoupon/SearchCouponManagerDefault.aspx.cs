﻿using BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.Manager.SearchCoupon
{
    public partial class SearchCouponManagerDefault : System.Web.UI.Page
    {
        DataTable _dt;
        IBL_Manager requests;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SiteMapDataSource1 != null)
            {
                SiteMapDataSource1.StartingNodeUrl = "~/Manager/ManagerHomeP.aspx";
            }
            _dt = new DataTable();
            requests = new BL_Manager();
            _dt = requests.selectCoupons((String)Session["UserName"]);
            GridView1.DataSource = _dt;
            GridView1.DataBind();
        }
    }
}