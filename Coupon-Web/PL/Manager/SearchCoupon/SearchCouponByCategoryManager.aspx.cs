﻿using BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.Manager.SearchCoupon
{
    public partial class SearchCouponByCategoryManager : System.Web.UI.Page
    {
        DataTable _dt;
        IBL_Manager managerController;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (SiteMapDataSource1 != null)
            {
                SiteMapDataSource1.StartingNodeUrl = "~/Manager/ManagerHomeP.aspx";
            }
            managerController = new BL_Manager();
            String user = (String)Session["User"];
            if (user.CompareTo("Manager") == 0)
            {
                _dt = managerController.selectCouponDetailsWithCategory(category.Text, (String)Session["UserName"]);
                View.DataSource = _dt;
                View.DataBind();
            }
            else
            {
                _dt = managerController.selectCouponDetailsWithCategory(category.Text, "");
                View.DataSource = _dt;
                View.DataBind();
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            String user = (String)Session["User"];
            if (user.CompareTo("Manager") == 0)
            {
                _dt = managerController.selectCouponDetailsWithCategory(category.Text, (String)Session["UserName"]);
                View.DataSource = _dt;
                View.DataBind();
            }
            else
            {
                _dt = managerController.selectCouponDetailsWithCategory(category.Text, "");
                View.DataSource = _dt;
                View.DataBind();
            }
        }

    }
}