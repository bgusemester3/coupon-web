﻿using BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.Manager
{
    public partial class AddCouponManager : System.Web.UI.Page
    {
        IBL_Manager managerController;

        protected void Page_Load(object sender, EventArgs e)
        {
            managerController = new BL_Manager();
            if (SiteMapDataSource1 != null)
            {
               SiteMapDataSource1.StartingNodeUrl = "~/Manager/ManagerHomeP.aspx";
            }
        }

        protected void addCoupon_btn_Click(object sender, EventArgs e)
        {
            string nextPage = SiteMapDataSource1.StartingNodeUrl;

            string couponName = txtBoxCouponName.Text;
            double couponPrice = double.Parse(txtBoxCouponPrice.Text);
            String getDate = (ddl_month.Text + "-" + ddl_day.Text + "-" + ddl_year.Text);
            int bussinessId = int.Parse(txtBoxBusinessID.Text);
            managerController.InsertCoupon(couponName, couponPrice, couponPrice, getDate, 1, bussinessId, DropDownList1.SelectedIndex + 1);
            Response.Redirect(nextPage);
        }

    }
}