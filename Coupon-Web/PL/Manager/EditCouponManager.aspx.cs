﻿using BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.Manager
{
    public partial class EditCouponManager : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SiteMapDataSource1 != null)
            {
                    SiteMapDataSource1.StartingNodeUrl = "~/Manager/ManagerHomeP.aspx";
            }
        }

        protected void addCoupon_btn_Click(object sender, EventArgs e)
        {
            IBL_Manager managerController = new BL_Manager();
            int couponId;
            try
            {
                couponId = int.Parse(txtBoxCouponId.Text);
                String manager = managerController.selectManagerOfCoupon(couponId);
                String user = (String)Session["User"];
                if (manager.CompareTo(Session["UserName"]) == 0 || user.CompareTo("System Manager") == 0)
                {
                    managerController.EditCoupon(couponId, DropDownList1.Text, TextBox1.Text);
                    Response.Redirect(SiteMapDataSource1.StartingNodeUrl);
                }
                else
                    errorlbl.Text = "You Don't Have Permissions To Change The Coupon";
            }
            catch (Exception)
            {
                errorlbl.Text = "Error,Please Check Out Your Parameters";
            }
        }
    }
}