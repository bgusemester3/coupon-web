﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Coupon_Web.BL;
using BL;

namespace PL
{
    public partial class RegisterPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ILogInController loginController = new LogInController();
            String userName = TbUserName.Text;
            bool isExist = loginController.IsUserExist(userName);
            if (isExist)
            {
                errorlbl.Text = "UserName Exist";
            }
            else
            {
                loginController.InsertCustomer(userName, TbEmail.Text, TbPhoneNumber.Text, TbName.Text, TbPassword.Text);
                Response.Redirect("~/LogIn/LoginPage.aspx");
            }
        }

        protected void Reset_Click(object sender, EventArgs e)
        {
            TbUserName.Text = "";
            TbName.Text = "";
            TbRepeatPass.Text = "";
            TbPassword.Text = "";
        }

        protected void PasswordVali2_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (args.Value.Length > 5)
                args.IsValid = true;
            else
                args.IsValid = false;
        }
    }
}