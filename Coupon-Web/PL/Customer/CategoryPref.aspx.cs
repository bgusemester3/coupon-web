﻿using BL;
using Coupon_Web.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.Customer
{
    public partial class CategoryPref : System.Web.UI.Page
    {
        private IBL_Customer customerController;
        private DataTable _dt;
       

        protected void Page_Load(object sender, EventArgs e)
        {
            customerController = new BL_Customer();
            loadView();

            if (SiteMapDataSource1 != null)
            {
                SiteMapDataSource1.StartingNodeUrl = "~/Customer/CustomerHomeP.aspx";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string userName = (String)Page.Session["UserName"];
            int catId = int.Parse(removeCategory.SelectedValue.ToString());
            customerController.removeCustomerCategoryPref(userName, catId);
            loadView();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            string userName = (String)Page.Session["UserName"];
            int catId = int.Parse(addCategory.SelectedValue.ToString());
            customerController.addCustomerCategoryPref(userName, catId);
            loadView();

        }

        private void loadView(){
            _dt = customerController.selectCustomerCategoryPref((String)Page.Session["UserName"]);
            View.DataSource = _dt;
            View.DataBind();
        }


    }
}