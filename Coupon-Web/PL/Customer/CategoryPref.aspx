﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer/CustomerMaster.Master" AutoEventWireup="true" CodeBehind="CategoryPref.aspx.cs" Inherits="PL.Customer.CategoryPref" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style6 {
            width: 311px;
            height: 83px;
        }
        .auto-style7 {
            height: 83px;
        }
        .auto-style8 {
            width: 311px;
            height: 6px;
        }
        .auto-style10 {
            width: 311px;
        }
        .auto-style13 {
            height: 44px;
        }
        .auto-style21 {
            height: 35px;
        }
        .auto-style32 {
            height: 35px;
            width: 462px;
        }
        .auto-style33 {
            height: 44px;
            width: 462px;
        }
        .auto-style35 {
            width: 462px;
        }
        .auto-style40 {
            height: 35px;
            width: 147px;
        }
        .auto-style41 {
            height: 44px;
            width: 147px;
        }
        .auto-style43 {
            width: 147px;
        }
        .auto-style44 {
            height: 12px;
            width: 462px;
        }
        .auto-style45 {
            height: 12px;
            width: 147px;
        }
        .auto-style46 {
            height: 12px;
        }
        .auto-style47 {
            height: 6px;
            width: 462px;
        }
        .auto-style48 {
            height: 6px;
            width: 147px;
        }
        .auto-style49 {
            height: 6px;
        }
        .auto-style50 {
            height: 18px;
            width: 462px;
        }
        .auto-style51 {
            height: 18px;
            width: 147px;
        }
        .auto-style52 {
            height: 18px;
        }
        .auto-style53 {
            height: 13px;
            width: 462px;
        }
        .auto-style54 {
            height: 13px;
            width: 147px;
        }
        .auto-style55 {
            height: 13px;
        }
                body { 
    background-image: url('/pictures/back.jpg');
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-position: center; 
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%; height: 448px;">
        <tr>
            <td class="auto-style7">
                &nbsp;</td>
            <td class="auto-style7">
                <asp:Label ID="Label16" runat="server" Text="Customer Preferences:" Width="272px" style="direction: ltr; text-decoration: underline; font-weight: 700; margin-bottom: 6px;" Font-Size="Larger"></asp:Label>
            </td>
            <td class="auto-style7"></td>
        </tr>
        <tr>
            <td class="auto-style47">
            </td>
            <td class="auto-style48">
                <asp:Label ID="Label18" runat="server" Text="Your's category preference:" Width="286px" style="font-weight: 700; text-decoration: underline"></asp:Label>
            </td>
            <td class="auto-style49"></td>
        </tr>
        <tr>
            <td class="auto-style44">
                <asp:SiteMapPath ID="SiteMapPath1" runat="server" Font-Names="Verdana" Font-Size="0.8em" PathSeparator=" : ">
                    <CurrentNodeStyle ForeColor="#333333" />
                    <NodeStyle Font-Bold="True" ForeColor="#990000" />
                    <PathSeparatorStyle Font-Bold="True" ForeColor="#990000" />
                    <RootNodeStyle Font-Bold="True" ForeColor="#FF8000" />
                </asp:SiteMapPath>
                <asp:TreeView ID="TreeView1" runat="server" DataSourceID="SiteMapDataSource1" ImageSet="Arrows">
                    <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                    <NodeStyle Font-Names="Tahoma" Font-Size="10pt" ForeColor="Black" HorizontalPadding="5px" NodeSpacing="0px" VerticalPadding="0px" />
                    <ParentNodeStyle Font-Bold="False" />
                    <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" HorizontalPadding="0px" VerticalPadding="0px" />
                </asp:TreeView>
                <asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" />
            </td>
            <td class="auto-style45">
                            <asp:GridView ID="View" runat="server" Width="185px" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
                                <AlternatingRowStyle BackColor="#DCDCDC" />
                                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#000065" />
                            </asp:GridView>
                        </td>
            <td class="auto-style46"></td>
        </tr>
        <tr>
            <td class="auto-style35">
            </td>
            <td class="auto-style43">
                <asp:Label ID="Label15" runat="server" Text="Choose the preference category to add:" Width="286px" style="font-weight: 700; text-decoration: underline"></asp:Label>
            </td>
            <td></td>
        </tr>
        <tr>
            <td class="auto-style47">
            </td>
            <td class="auto-style48">
                <asp:DropDownList ID="addCategory" runat="server" AutoPostBack="True" ValidationGroup="aaa" DataSourceID="SqlDataSource1" DataTextField="Name" DataValueField="Id">
                    <asp:ListItem>Choose ID</asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT [Name], [Id] FROM [Category]"></asp:SqlDataSource>
            </td>
            <td class="auto-style49"></td>
        </tr>
        <tr>
            <td class="auto-style50">
            </td>
            <td class="auto-style51"></td>
            <td class="auto-style52">
                <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Add the seleced category" Width="186px" />
            </td>
        </tr>
        <tr>
            <td class="auto-style32">&nbsp;</td>
            <td class="auto-style40">
                <asp:Label ID="Label17" runat="server" Text="Choose the preference category to remove:" Width="308px" style="font-weight: 700; text-decoration: underline"></asp:Label>
            </td>
            <td class="auto-style21"></td>
        </tr>
        <tr>
            <td class="auto-style33">
            </td>
            <td class="auto-style41">
                <asp:DropDownList ID="removeCategory" runat="server" AutoPostBack="True" ValidationGroup="aaa" DataSourceID="SqlDataSource1" DataTextField="Name" DataValueField="Id">
                    <asp:ListItem>Choose ID</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="auto-style13"></td>
        </tr>
        <tr>
            <td class="auto-style53">
            </td>
            <td class="auto-style54"></td>
            <td class="auto-style55">
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Remove the seleced category" Width="188px" />
            </td>
        </tr>
        <tr>
            <td class="auto-style35"></td>
            <td class="auto-style43">
                &nbsp;</td>
            <td></td>
        </tr>
    </table>
</asp:Content>
