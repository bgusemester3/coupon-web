﻿using BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.Customer.SearchCoupon
{
    public partial class SearchCouponByPreferedCategoryCutomer : System.Web.UI.Page
    {
        private DataTable _dtCoupons;
        private DataTable _dtCategories;
        private BL_Customer customerController;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (SiteMapDataSource1 != null)
                {
                    SiteMapDataSource1.StartingNodeUrl = "~/Customer/CustomerHomeP.aspx";
                }

                customerController = new BL_Customer();
                String userName = (String)Session["UserName"];
                _dtCategories = customerController.selectCustomerCategoryPref(userName);
                DropDownList1.DataSource = _dtCategories;
                DropDownList1.DataTextField = "Category";
                DropDownList1.DataValueField = "Category";
                DropDownList1.DataBind();
                _dtCoupons = customerController.selectCouponsWithCategory(DropDownList1.SelectedValue);
                GridView1.DataSource = _dtCoupons;
                GridView1.DataBind();
            }
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            customerController = new BL_Customer();
            _dtCoupons = customerController.selectCouponDetailsWithCategory(DropDownList1.SelectedValue, "");
            GridView1.DataSource = _dtCoupons;
            GridView1.DataBind();
        }


            
    }
}