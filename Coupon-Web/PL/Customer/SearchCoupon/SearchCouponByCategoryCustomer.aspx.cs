﻿using BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.Customer.SearchCoupon
{
    public partial class SearchCouponByCategoryCustomer : System.Web.UI.Page
    {
        private DataTable _dt;
        private BL_Customer customerController;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (SiteMapDataSource1 != null)
            {
               SiteMapDataSource1.StartingNodeUrl = "~/Customer/CustomerHomeP.aspx";
            }
            customerController = new BL_Customer();
            String user = (String)Session["User"];
            if (user.CompareTo("Manager") == 0)
            {
                _dt = customerController.selectCouponDetailsWithCategory(category.Text, (String)Session["UserName"]);
                View.DataSource = _dt;
                View.DataBind();
            }
            else
            {
                _dt = customerController.selectCouponDetailsWithCategory(category.Text, "");
                View.DataSource = _dt;
                View.DataBind();
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            String user = (String)Session["User"];
            if (user.CompareTo("Manager") == 0)
            {
                _dt = customerController.selectCouponDetailsWithCategory(category.Text, (String)Session["UserName"]);
                View.DataSource = _dt;
                View.DataBind();
            }
            else
            {
                _dt = customerController.selectCouponDetailsWithCategory(category.Text, "");
                View.DataSource = _dt;
                View.DataBind();
            }
        }
        
    }
}