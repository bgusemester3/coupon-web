﻿using BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.Customer.SearchCoupon
{
    public partial class SearchCouponByLocationCustomer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (SiteMapDataSource1 != null)
            {
                SiteMapDataSource1.StartingNodeUrl = "~/Customer/CustomerHomeP.aspx";
            }
        }

        public string ConvertDataTabletoString()
        {
            IBL_Customer customerController = new BL_Customer();
            DataTable dt;
            String user = (String)Session["User"];
            if (user.CompareTo("Manager") == 0)
            {
                dt = customerController.selectCoupons((String)Session["UserName"]);
            }
            else
            {
                dt = customerController.selectCoupons("");
            }
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

    }

}