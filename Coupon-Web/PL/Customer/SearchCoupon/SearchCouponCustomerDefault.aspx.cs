﻿using BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.Customer.SearchCoupon
{
    public partial class SearchCouponCustomerDefault : System.Web.UI.Page
    {
        DataTable _dt;
        IBL_Customer requests;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SiteMapDataSource1 != null)
            {
                SiteMapDataSource1.StartingNodeUrl = "~/Customer/CustomerHomeP.aspx";
            }
            _dt = new DataTable();
            requests = new BL_Customer();
            _dt = requests.selectAllCoupons();
            GridView1.DataSource = _dt;
            GridView1.DataBind();
        }
    }
}