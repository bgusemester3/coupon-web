﻿using BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PL.Customer.SearchCoupon
{
    public partial class SearchCouponByNameCustomer : System.Web.UI.Page
    {
        DataTable _dt;
        IBL_Customer customerController;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (SiteMapDataSource1 != null)
            {
               SiteMapDataSource1.StartingNodeUrl = "~/Customer/CustomerHomeP.aspx";
            }
            _dt = new DataTable();
            customerController = new BL_Customer();
            String user = (String)Session["User"];
            if (user.CompareTo("Manager") == 0)
            {
                _dt = customerController.selectCouponsDetails((String)Session["UserName"]);
                View.DataSource = _dt;
                View.DataBind();
            }
            else
            {
                _dt = customerController.selectCouponsDetails("");
                View.DataSource = _dt;
                View.DataBind();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string text = TextBox_Name.Text;
            if (text.CompareTo("") == 0)
                _dt = customerController.selectCouponsDetails("");
            else 
            _dt = customerController.selectCouponsName(text);
            View.DataSource = _dt;
            View.DataBind();
        }

    }
}